﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.TemplateMethod.TheoryCode
{
    class Client
    {
        public void Run(AbstractClass ac)
        {
            ac.TemplateMethod();
        }
    }

    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            Client c = new Client();

            c.Run(new ConcreteClassA());

            Console.WriteLine();

            c.Run(new ConcreteClassB());

            // Wait for user
            Console.Read();
        }
    }

    // "AbstractClass"

    abstract class AbstractClass
    {
        protected abstract void PrimitiveOperation1();
        protected abstract void PrimitiveOperation2();

        protected virtual bool IsValid()
        {
            return true;
        }

        // The "Template method"
        public void TemplateMethod()
        {
            PrimitiveOperation1();

            if (IsValid())
                PrimitiveOperation2();

            Console.WriteLine("");
        }
    }

    // "ConcreteClass"

    class ConcreteClassA : AbstractClass
    {
        protected override void PrimitiveOperation1()
        {
            Console.WriteLine("ConcreteClassA.PrimitiveOperation1()");
        }

        protected override void PrimitiveOperation2()
        {
            Console.WriteLine("ConcreteClassA.PrimitiveOperation2()");
        }
    }

    class ConcreteClassB : AbstractClass
    {
        protected override void PrimitiveOperation1()
        {
            Console.WriteLine("ConcreteClassB.PrimitiveOperation1()");
        }

        protected override void PrimitiveOperation2()
        {
            Console.WriteLine("ConcreteClassB.PrimitiveOperation2()");
        }

        protected override bool IsValid()
        {
            Random random = new Random();

            if (random.Next(100) < 50)
            {
                return true;
            }

            return false;
        }
    }
}
