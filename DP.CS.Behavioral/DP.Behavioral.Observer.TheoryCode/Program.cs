﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Observer.TheoryCode
{
    /// <summary>
    /// Observer Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            // Configure Observer pattern
            ConcreteSubject s = new ConcreteSubject();
            Observer o = new ConcreteObserver(s, "X");

            s.Attach(o);
            s.Attach(new ConcreteObserver(s, "Y"));
            s.Attach(new ConcreteObserver(s, "Z"));

            // Change subject and notify observers
            s.SubjectState = "ABC";

            // Wait for user
            Console.WriteLine("Wait for user...");
            Console.ReadKey();

            s.Detach(o);
            
            s.SubjectState = "DEF";

            Console.WriteLine("\n.NET version of observer: ");
            
            Publisher publisher = new Publisher("Publisher1");

            Subscriber s1 = new Subscriber("Subscriber1");
            Subscriber s2 = new Subscriber("Subscriber2");

            publisher.DataChanged += new EventHandler<CustomEventArgs>(s1.Update);
            publisher.DataChanged += new EventHandler<CustomEventArgs>(s2.Update);

            publisher.Use();

            publisher.DataChanged -= s2.Update;

            Console.WriteLine();

            publisher.Use();
        }
    }

    // "Subject" 
    abstract class Subject
    {
        private ArrayList observers = new ArrayList();

        public void Attach(Observer observer)
        {
            observers.Add(observer);
        }

        public void Detach(Observer observer)
        {
            observers.Remove(observer);
        }

        protected void Notify()
        {
            foreach (Observer o in observers)
            {
                o.Update();
            }
        }
    }

    // "ConcreteSubject" 
    class ConcreteSubject : Subject
    {
        private string subjectState;

        // Property
        public string SubjectState
        {
            get { return subjectState; }
            set 
            {
                subjectState = value;
                Notify();
            }
        }
    }

    // "Observer" 
    abstract class Observer
    {
        public abstract void Update();
    }

    // "ConcreteObserver" 
    class ConcreteObserver : Observer
    {
        private string name;
        private string observerState;
        private ConcreteSubject subject;

        // Constructor
        public ConcreteObserver(
            ConcreteSubject subject, string name)
        {
            this.subject = subject;
            this.name = name;
        }

        public override void Update()
        {
            observerState = subject.SubjectState;
            Console.WriteLine("Observer {0}'s new state is {1}",
                name, observerState);
        }

        // Property
        public ConcreteSubject Subject
        {
            get { return subject; }
            set { subject = value; }
        }
    }

    // .NET observer

    class CustomEventArgs : EventArgs
    {
        public int Data { get; set; }
    }

    class Publisher
    {
        public event EventHandler<CustomEventArgs> DataChanged;
        public string Name { get; set; }

        private int data;

        public Publisher(string name)
        {
            Name = name;
        }

        public void Use()
        {
            data++;

            if (DataChanged != null)
            {
                DataChanged(this, new CustomEventArgs { Data = data });
            }
        }
    }

    class Subscriber
    {
        public string Name { get; set; }

        public Subscriber(string name)
        {
            Name = name;
        }

        public void Update(object sender, CustomEventArgs args)
        {
            Publisher p = (Publisher)sender;
            Console.WriteLine("{0} notified by {1} - data changed: {2}", Name, p.Name, args.Data);
        }
    }
}
