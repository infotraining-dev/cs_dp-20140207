﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Drawing;
using System.Xml.Linq;
using System.IO;

namespace DP.Creational.FactoryMethod.Example
{

    public class CsvVisitor : IVisitor
    {
        StreamWriter writer;

        public CsvVisitor(string fileName)
        {
            writer = new StreamWriter(fileName);
        }

        #region IVisitor Members

        public void Visit(Circle c)
        {
            writer.WriteLine("Circle; {0}; {1}; ", c.GetPoint(0), c.Radius);
        }

        public void Visit(Line l)
        {
            writer.WriteLine("Line; {0}; {1};", l.GetPoint(0), l.GetPoint(1));
        }

        public void Visit(Rectangle r)
        {
            writer.WriteLine("Rectangle; {0}; {1}; {2};", r.GetPoint(0), r.Height, r.Width);
        }

        public void Visit(Image i)
        {
            writer.WriteLine("Image; {0}; {1};", i.GetPoint(0), i.Path);
        }

        public void Visit(Text t)
        {
            writer.WriteLine("Text; {0}; {1}; ", new Point(t.PosX, t.PosY), t.Text);
        }

        public void Close()
        {
            writer.Close();
        }

        #endregion
    }

    public class Document
    {
        List<IShape> shapes = new List<IShape>();

        public virtual void Load(string path)
        {
            XElement root = XElement.Load(path);

            foreach (XElement element in root.Elements("Shape"))
            {
                string id = element.Attribute("Id").Value;
                try
                {
                    IShape s = Shape.Factory.Create(id, element);
                    shapes.Add(s);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void Draw()
        {
            foreach (IShape s in shapes)
            {
                s.Draw();
            }
        }

        public void SaveCsv(string path)
        {
            CsvVisitor csv = new CsvVisitor(path);

            foreach (IShape s in shapes)
                s.Accept(csv);

            csv.Close();
        }
    }

    class Program
    {
        static void Init()
        {
            Shape.Factory.Register("Circle", new Circle.Factory());
            Shape.Factory.Register("Rectangle", new Rectangle.Factory());
            Shape.Factory.Register("Image", new Image.Factory());
            Shape.Factory.Register("Line", new Line.Factory());
            Shape.Factory.Register("Text", new Text.Factory());
        }

        static void Main(string[] args)
        {
            Init();
            Document doc = new Document();
            doc.Load("../../graphics.xml");
            doc.Draw();
            doc.SaveCsv("../../graphics.csv");
        }
    }
}
