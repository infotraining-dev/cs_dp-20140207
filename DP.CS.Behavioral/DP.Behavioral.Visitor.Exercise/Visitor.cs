﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawing
{
    public interface IVisitor
    {
        void Visit(Circle c);
        void Visit(Line l);
        void Visit(Rectangle r);
        void Visit(Image i);
        void Visit(Text t);
    }
}
