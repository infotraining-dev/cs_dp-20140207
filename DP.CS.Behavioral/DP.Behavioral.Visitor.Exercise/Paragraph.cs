﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LegacyCode
{
    [Serializable()]
    public class Paragraph
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string Text { get; set; }

        public Paragraph()
        {
            Text = "Default Text";
        }

        public Paragraph(int posX, int posY, string text)
        {
            PosX = posX;
            PosY = posY;
            Text = text;
        }

        public void Render()
        {
            Console.WriteLine("Rendering text at [{0}, {1}]: {2}", PosX, PosY, Text);
        }
    }
}
