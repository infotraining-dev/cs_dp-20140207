﻿namespace Tokenizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Open = new System.Windows.Forms.Button();
            this.codeText = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Open
            // 
            this.Open.Location = new System.Drawing.Point(207, 249);
            this.Open.Name = "Open";
            this.Open.Size = new System.Drawing.Size(75, 23);
            this.Open.TabIndex = 2;
            this.Open.Text = "Open";
            this.Open.Click += new System.EventHandler(this.Open_Click);
            // 
            // codeText
            // 
            this.codeText.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeText.Location = new System.Drawing.Point(4, 13);
            this.codeText.Name = "codeText";
            this.codeText.Size = new System.Drawing.Size(480, 224);
            this.codeText.TabIndex = 3;
            this.codeText.Text = "";
            this.codeText.TextChanged += new System.EventHandler(this.codeText_TextChanged);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(496, 273);
            this.Controls.Add(this.codeText);
            this.Controls.Add(this.Open);
            this.Name = "Form1";
            this.Text = "Color Syntax";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Open;
        private System.Windows.Forms.RichTextBox codeText;

    }
}

