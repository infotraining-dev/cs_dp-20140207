﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.State.Example
{
    interface ITurnstileState
    {
        void Coin(Turnstile t);
        void Pass(Turnstile t);
    }

    class TurnstileLocked : ITurnstileState
    {
        #region ITurnstilleState Members

        public void Coin(Turnstile t)
        {
            t.SetState(new TurnstileUnlocked());
            t.Unlock();
        }

        public void Pass(Turnstile t)
        {
            t.Alarm();
        }

        #endregion
    }

    class TurnstileUnlocked : ITurnstileState
    {
        #region ITurnstilleState Members

        public void Coin(Turnstile t)
        {
            t.ThankYou();
        }

        public void Pass(Turnstile t)
        {
            t.SetState(new TurnstileLocked());
            t.Lock();
        }

        #endregion
    }

    class Turnstile
    {
        ITurnstileState state;

        public Turnstile(ITurnstileState ts)
        {
            state = ts;
        }

        public void SetState(ITurnstileState ts)
        {
            state = ts;
        }

        public void Lock()
        {
            Console.WriteLine("LOCKED");
        }

        public void Unlock()
        {
            Console.WriteLine("UNLOCKED");
        }

        public void Alarm()
        {
            Console.WriteLine("ALARM");
        }

        public void ThankYou()
        {
            Console.WriteLine("THANK YOU");
        }

        public void Coin()
        {
            state.Coin(this);
        }

        public void Pass()
        {
            state.Pass(this);
        }
    }

    class Program
    {
        public static void Main()
        {
            Turnstile t = new Turnstile(new TurnstileLocked());

            t.Coin();
            t.Pass();
            t.Pass();
            t.Coin();
            t.Coin();
            t.Pass();
            t.Pass();
        }
    }
}
