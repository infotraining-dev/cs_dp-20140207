﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace DP.Behavioral.TemplateMethod.Example
{

    abstract class LoginComponent
    {
        protected abstract void ProvideUserPassword(out string user, out string passwd);
        protected abstract bool Authenticate(string user, string password);

        public bool Login()
        {
            string user, password;
            ProvideUserPassword(out user, out password);
            authenticated = Authenticate(user, password);
            return authenticated;
        }

        protected bool authenticated;
        public bool Authenticated
        {
            get { return authenticated; }
        }
    }

    class ConsoleLogin : LoginComponent
    {
        protected override void ProvideUserPassword(out string user, out string password)
        {
            Console.Write("User name: ");
            user = Console.ReadLine();
            Console.Write("Password: ");
            password = Console.ReadLine();
        }

        protected override bool Authenticate(string user, string password)
        {
            if ((user == "admin") && (password == "passwd"))
            {
                Console.WriteLine("Access Granted");
                return true;
            }
            else
            {
                Console.WriteLine("Access Denied");
                return false;
            }
        }
    }

    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            LoginComponent login = new ConsoleLogin();

            while (!login.Login()) ;

            if (login.Authenticated)
                Console.WriteLine("User has been authenticated");

        }
    }
}
