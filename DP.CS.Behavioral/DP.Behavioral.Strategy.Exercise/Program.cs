﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DP.Behavioral.Strategy.Exercise
{
    public struct Result
    {
        public string Description;
        public double Value;

        public Result(string desc, double val)
        {
            Description = desc;
            Value = val;
        }
    }

    public enum StatType
    {
        Avg, Sum, Mediana
    }

    public interface IStatistics
    {
        void Calculate(IList<double> data, IList<Result> results);
    }

    public class StatGroup : IStatistics
    {
        IList<IStatistics> _stats = new List<IStatistics>();

        public void Add(IStatistics stat)
        {
            _stats.Add(stat);
        }

        public void Calculate(IList<double> data, IList<Result> results)
        {
            foreach(var stat in _stats)
                stat.Calculate(data, results);
        }
    }

    public class Avg : IStatistics
    {
        public void Calculate(IList<double> data, IList<Result> results)
        {
            double avg = data.Average();
            results.Add(new Result("AVG", avg));
        }
    }

    public class Sum : IStatistics
    {
        public void Calculate(IList<double> data, IList<Result> results)
        {
            double sum = data.Sum();

            results.Add(new Result("SUM", sum));
        }
    }

    public class Mediana : IStatistics
    {
        public void Calculate(IList<double> data, IList<Result> results)
        {
            double mediana;

            double[] values = data.OrderBy(v => v).ToArray();
            
            int length = values.Length;

            if (length % 2 != 0)
                mediana = values[(length + 1) / 2];
            else
                mediana = (values[length / 2] + values[length / 2 + 1]) / 2;

            results.Add(new Result("MEDIANA", mediana));
        }
    }

    public interface IDataParser
    {
        IEnumerable<double> ParseFile(string path);
    }

    public class TextFileParser : IDataParser
    {
        public IEnumerable<double> ParseFile(string path)
        {
            List<double> values = new List<double>();

            using (StreamReader reader = File.OpenText(path))
            {
                string line = reader.ReadToEnd();

                foreach (string token in line.Split(' '))
                    values.Add(double.Parse(token));
            }

            return values;
        }
    }

    public class DataAnalyzer
    {
        private double[] _data;
        readonly List<Result> _results = new List<Result>();
        public IStatistics Statistics { get; set; }

        public DataAnalyzer(IStatistics statistics)
        {
            Statistics = statistics;
        }

        public IDataParser DataParser { get; set; }

        public virtual void LoadData(string path)
        {
            _data = DataParser.ParseFile(path).ToArray();

            _results.Clear();
        }

        public void CalculateStat()
        {
            Statistics.Calculate(_data, _results);
        }

        public void ShowStatistics()
        {
            Console.Write("Data: [ ");

            foreach (double d in _data)
                Console.Write("{0} ", d);
            Console.WriteLine("]");

            Console.WriteLine("\nStatistics for data:");
            foreach (var result in _results)
                Console.WriteLine(" + {0} = {1}", result.Description, result.Value);
        }   
    }

    class Program
    {
        public static void Main()
        {
            StatGroup statGroup = new StatGroup();
            statGroup.Add(new Avg());
            statGroup.Add(new Sum());
            statGroup.Add(new Mediana());

            DataAnalyzer dataAnalizer = new DataAnalyzer(statGroup);
            dataAnalizer.DataParser = new TextFileParser();
            dataAnalizer.LoadData("test.dat");
            dataAnalizer.CalculateStat();

            dataAnalizer.ShowStatistics();
        }
    }
}
