﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace DP.Behavioral.State.Exercise
{
    /// <summary>
    /// State Design Pattern.
    /// </summary>

    enum State { SOLD_OUT, NO_QUARTER, HAS_QUARTER, SOLD };

    interface IState
    {
        IState InsertQuarter();
        IState EjectQuarter();
        IState TurnCrank();
        IState Dispense(ref int count);
        string GetStatus();
    }

    class SoldOut : IState
    {
        public IState InsertQuarter()
        {
            Console.WriteLine("You can't insert a quarter, the machine is sold out");
            return this;
        }

        public IState EjectQuarter()
        {
            Console.WriteLine("You can't eject, you haven't inserted a quarter yet");
            return this;
        }

        public IState TurnCrank()
        {
            Console.WriteLine("You turned, but there are no gumballs");
            return this;
        }

        public IState Dispense(ref int count)
        {
            Console.WriteLine("No gumball dispensed");
            return this;
        }

        public string GetStatus()
        {
            return "sold out\n";
        }
    }

    class NoQuarter : IState
    {
        public IState InsertQuarter()
        {
            Console.WriteLine("You inserted a quarter");

            return new HasQuarter();
        }

        public IState EjectQuarter()
        {
            Console.WriteLine("You haven't inserted a quarter");

            return this;
        }

        public IState TurnCrank()
        {
            Console.WriteLine("You turned but there's no quarter");

            return this;
        }

        public IState Dispense(ref int count)
        {
            Console.WriteLine("You need to pay first");

            return this;
        }

        public string GetStatus()
        {
            return "waiting for quarter\n";
        }
    }

    class HasQuarter : IState
    {
        public IState InsertQuarter()
        {
            Console.WriteLine("You can't insert another quarter");

            return this;
        }

        public IState EjectQuarter()
        {
            Console.WriteLine("Quarter returned");
            return new NoQuarter();
        }

        public IState TurnCrank()
        {
            Console.WriteLine("You turned...");
            return new Sold();
        }

        public IState Dispense(ref int count)
        {
            Console.WriteLine("No gumball dispensed");
            return this;
        }

        public string GetStatus()
        {
            return "waiting for turn of crank\n";
        }
    }

    class Sold : IState
    {
        public IState InsertQuarter()
        {
            Console.WriteLine("Please wait, we're already giving you a gumball");

            return this;
        }

        public IState EjectQuarter()
        {
            Console.WriteLine("Sorry, you already turned the crank");

            return this;
        }

        public IState TurnCrank()
        {
            Console.WriteLine("Turning twice doesn't get you another gumball!");

            return this;
        }

        public IState Dispense(ref int count)
        {
            Console.WriteLine("A gumball comes rolling out the slot");
            count = count - 1;
            if (count == 0)
            {
                Console.WriteLine("Oops, out of gumballs!");
                return new SoldOut();
            }
            else
            {
                return new NoQuarter();
            }
        }

        public string GetStatus()
        {
           return "delivering a gumball\n";
        }
    }


    class GumballMachine
    {
        IState _state = new SoldOut();
        int _count = 0;

        public GumballMachine(int count)
        {
            this._count = count;

            if (this._count > 0)
                _state = new NoQuarter();
        }

        public void InsertQuarter()
        {
            _state = _state.InsertQuarter();
        }

        public void EjectQuarter()
        {
            _state = _state.EjectQuarter();
        }

        public void TurnCrank()
        {
            _state = _state.TurnCrank();
            Dispense();
        }

        public void Dispense()
        {
            _state = _state.Dispense(ref _count);
        }

        public void Refill(int numGumBalls)
        {
            this._count = numGumBalls;
            _state = new NoQuarter();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nMighty Gumball, Inc.");
            sb.Append("\n.NET Standing Gumball Model #2010\n");
            sb.Append("Inventory: " + _count + " gumball");

            if (_count != 1)
            {
                sb.Append("s");
            }

            sb.Append("\nMachine is ");
            sb.Append(_state.GetStatus());

            return sb.ToString();
        }
    }

    class Program
    {
        public static void Main()
        {
            GumballMachine gumballMachine = new GumballMachine(5);

            Console.WriteLine(gumballMachine);

            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();

            Console.WriteLine(gumballMachine);

            gumballMachine.InsertQuarter();
            gumballMachine.EjectQuarter();
            gumballMachine.TurnCrank();

            Console.WriteLine(gumballMachine);

            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();
            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();
            gumballMachine.EjectQuarter();

            Console.WriteLine(gumballMachine);

            gumballMachine.InsertQuarter();
            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();
            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();
            gumballMachine.InsertQuarter();
            gumballMachine.TurnCrank();

            Console.WriteLine(gumballMachine);
        }
    }
}
