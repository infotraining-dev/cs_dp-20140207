﻿namespace DP.Behavioral.Mediator.Example
{
    partial class CompanyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.lbxCompany = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(12, 12);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(260, 20);
            this.txtCompanyName.TabIndex = 0;
            // 
            // lbxCompany
            // 
            this.lbxCompany.FormattingEnabled = true;
            this.lbxCompany.Items.AddRange(new object[] {
            "Bombardier",
            "Infotraining",
            "Motorola",
            "PKP"});
            this.lbxCompany.Location = new System.Drawing.Point(13, 39);
            this.lbxCompany.Name = "lbxCompany";
            this.lbxCompany.Size = new System.Drawing.Size(259, 212);
            this.lbxCompany.TabIndex = 1;
            // 
            // CompanyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.lbxCompany);
            this.Controls.Add(this.txtCompanyName);
            this.Name = "CompanyForm";
            this.Text = "CompanyForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.ListBox lbxCompany;
    }
}