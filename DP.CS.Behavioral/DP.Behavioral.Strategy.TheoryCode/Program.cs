﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Strategy.TheoryCode
{
    /// <summary>
    /// Strategy Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            Context context;

            ConcreteStrategyA algA = new ConcreteStrategyA();
            // Three contexts following different strategies
            context = new Context(algA);
            context.ContextInterface();

            context.Strategy = new ConcreteStrategyB();
            context.ContextInterface();

            context.Strategy = new ConcreteStrategyC();
            context.ContextInterface();

            // Wait for user
            Console.Read();
        }
    }

    // "Strategy"
    interface IStrategy
    {
        void AlgorithmInterface();
    }

    // "ConcreteStrategyA"
    class ConcreteStrategyA : IStrategy
    {
        public void AlgorithmInterface()
        {
            Console.WriteLine(
                "Called ConcreteStrategyA.AlgorithmInterface()");
        }
    }

    // "ConcreteStrategyB"
    class ConcreteStrategyB : IStrategy
    {
        public void AlgorithmInterface()
        {
            Console.WriteLine(
                "Called ConcreteStrategyB.AlgorithmInterface()");
        }
    }

    // "ConcreteStrategyC"
    class ConcreteStrategyC : IStrategy
    {
        public void AlgorithmInterface()
        {
            Console.WriteLine(
                "Called ConcreteStrategyC.AlgorithmInterface()");
        }
    }

    // "Context" 
    class Context
    {
        public IStrategy Strategy { get; set; }

        // Constructor
        public Context(IStrategy strategy)
        {
            this.Strategy = strategy;
        }

        public void ContextInterface()
        {
            Strategy.AlgorithmInterface();  // wywolanie algorytmu
        }
    }
}
