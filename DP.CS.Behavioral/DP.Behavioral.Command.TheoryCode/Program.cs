﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Command.TheoryCode
{
    /// <summary>
    /// Command Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            // Create receiver, command, and invoker
            Receiver receiver = new Receiver();
            ICommand command = new ConcreteCommand(receiver);
            Invoker invoker = new Invoker();

            // Set and execute command
            invoker.SetCommand(command);
            invoker.ExecuteCommand();

            // Wait for user
            Console.Read();
        }
    }

    // "ICommand" 
    interface ICommand
    {        
        void Execute();
    }

    // "ConcreteCommand" 
    class ConcreteCommand : ICommand
    {
        protected Receiver receiver;

        // Constructor
        public ConcreteCommand(Receiver receiver)
        {
            this.receiver = receiver;
        }
        
        public virtual void Execute()
        {
            receiver.Action();
        }
    }

    // "Receiver"
    class Receiver
    {
        public void Action()
        {
            Console.WriteLine("Called Receiver.Action()");
        }
    }

    // "Invoker" 
    class Invoker
    {
        private ICommand command;

        public void SetCommand(ICommand command)
        {
            this.command = command;
        }

        public void ExecuteCommand()
        {
            command.Execute();
        }
    }
}
