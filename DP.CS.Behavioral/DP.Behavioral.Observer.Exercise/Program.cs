﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DP.Behavioral.Observer.Exercise
{
    // argumenty zdarzenia
    public class TemperatureEventArgs : EventArgs
    {
        public double Temperature { get; set; }

        public TemperatureEventArgs(double t)
        {
            Temperature = t;
        }
    }

    public class TemperatureMonitor
    {
        // deklaracja zdarzenia zmiany temperatury
        // TODO

        public void StartMonitoring()
        {
            Random rand = new Random(100);
            while (true)
            {
                double currentTemp = rand.NextDouble() * 100;

                // wyzwolenie zdarzenia z informacją o biezacej temp.
                // TODO
                Thread.Sleep(1500);                
            }
        }

        public void RunMonitor()
        {
            Thread t = new Thread(new ThreadStart(this.StartMonitoring));
            t.Start();
        }
    }

    class Fan
    {
        bool IsOn { get; set; }

        public void On()
        {
            Console.WriteLine("Fan.On()");
            IsOn = true;
        }

        public void Off()
        {
            Console.WriteLine("Fan.Off()");
            IsOn = false;
        }

        public void OnTempertureChanged(object sender, TemperatureEventArgs args)
        {
            if (args.Temperature >= 50 && !IsOn)
                On();
            else if (args.Temperature < 50 && IsOn)
                Off();
        }
    }

    class Program
    {
        static void PrintTemperature(object sender, TemperatureEventArgs args)
        {
            Console.WriteLine("Current temp: {0}", args.Temperature);
        }

        static void Main(string[] args)
        {
            TemperatureMonitor monitor = new TemperatureMonitor();
            Fan fan1 = new Fan();

            // Subskrybcja zdarzenia zmiany temp. przez PrintTemp i fan1
            // TODO

            monitor.RunMonitor();

            Thread.Sleep(10000);

            // Wyrejestrowanie PrintTemp z listy subskrybentow
            // TODO
        }
    }
}
