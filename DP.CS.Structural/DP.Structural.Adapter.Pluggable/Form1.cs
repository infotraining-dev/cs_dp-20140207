﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;

namespace DP.Structural.Adapter.Pluggable
{
    public partial class FormMain : Form
    {
        //TreeViewer<DirectoryInfo> treeViewer;
        TreeViewer<Control> treeViewer;
        //TreeViewer<XElement> treeViewer;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            //treeViewer = new TreeViewer<DirectoryInfo>(treeView);
            //ITreeContentProvider<DirectoryInfo> provider = new DirTreeContentProvider();
            //treeViewer.SetTreeContentProvider(provider);
            //treeViewer.BuildTree(null, new DirectoryInfo("D:\\VBox"));

            treeViewer = new TreeViewer<Control>(treeView);
            ITreeContentProvider<Control> provider = new ControlTreeContentProvider();
            treeViewer.SetTreeContentProvider(provider);
            treeViewer.BuildTree(null, this);

            //treeViewer = new TreeViewer<XElement>(treeView);
            //ITreeContentProvider<XElement> provider = new XmlTreeContentProvider();
            //treeViewer.SetTreeContentProvider(provider);
            //treeViewer.BuildTree(null, XElement.Load("../../graphics.xml"));
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
