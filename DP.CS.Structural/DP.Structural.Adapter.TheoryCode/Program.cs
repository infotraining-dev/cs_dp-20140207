﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Adapter.TheoryCode
{
    /// <summary>
    /// Adapter Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            Client client = new Client();

            // using ObjectAdapter
            Console.WriteLine("ObjectAdapter:");
            Adaptee adaptee = new Adaptee();
            ITarget target = new ObjectAdapter(adaptee);
            client.Run(target);
            

            // using ClassAdapter
            Console.WriteLine("ClassAdapter:");
            target = new ClassAdapter();
            client.Run(target);
        }
    }

    // "Target" 
    interface ITarget
    {
        void Request();
    }

    class Client
    {
        public void Run(ITarget obj)
        {
            obj.Request();
        }
    }

    // "Adaptee"
    class Adaptee
    {
        public void SpecificRequest()
        {
            Console.WriteLine("Called SpecificRequest()");
        }
    }

    // "Adapter" 
    class ObjectAdapter : ITarget
    {
        private Adaptee adaptee;

        public ObjectAdapter(Adaptee a)
        {
            adaptee = a;
        }

        public void Request()
        {
            // additional work possible
            adaptee.SpecificRequest();
        }
    }

    // "Adapter"
    class ClassAdapter : Adaptee, ITarget
    {
        #region ITarget Members

        public void Request()
        {
            this.SpecificRequest();
        }

        #endregion
    }
}
