﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Drawing;
using System.Xml.Linq;

namespace DP.Creational.FactoryMethod.Example
{
    public class Document
    {
        ShapeGroup shapes = new ShapeGroup();

        public virtual void Load(string path)
        {
            XElement root = XElement.Load(path);

            foreach (XElement element in root.Elements("Shape"))
            {
                string id = element.Attribute("Id").Value;
                try
                {
                    IShape s = ShapeFactory.Create(id, element);
                    shapes.Add(s);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void Draw()
        {
            shapes.Draw();
        }
    }

    class Program
    {
        static void Init()
        {
            ShapeFactory.Register("Circle", new Circle.Factory());
            ShapeFactory.Register("Rectangle", new Rectangle.Factory());
            ShapeFactory.Register("Image", new ImageProxy.Factory());
            ShapeFactory.Register("Line", new Line.Factory());
            ShapeFactory.Register("ShapeGroup", new ShapeGroup.Factory());
        }

        static void Main(string[] args)
        {
            Init();
            Document doc = new Document();
            doc.Load("../../graphics.xml");
            Console.WriteLine("Press a key to draw document...");
            Console.ReadKey();
            doc.Draw();
            Console.ReadKey();
            Console.WriteLine();
            doc.Draw();
        }
    }
}
