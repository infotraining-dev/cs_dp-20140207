﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    [Serializable()]
    public class Circle : Shape
    {
        public int Radius { get; set; }

        public Circle(int x, int y, int radius)
        {
            AddPoint(new Point(x, y));
            Radius = radius;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a circle at {0} with radius {1}", GetPoint(0), Radius);
        }

        public class Factory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement circleElement = (XElement)args[0];
                int x = int.Parse(circleElement.Element("Point").Element("X").Value);
                int y = int.Parse(circleElement.Element("Point").Element("Y").Value);
                int radius = int.Parse(circleElement.Element("Radius").Value);

                return new Circle(x, y, radius);

            }
        }
    }
}
