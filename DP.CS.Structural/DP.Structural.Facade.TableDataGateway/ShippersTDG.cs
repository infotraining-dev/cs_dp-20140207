﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DP.Structural.Facade.TableDataGateway
{
    public class Shipper
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
    }

    public interface IShipperRepository
    {
        void Add(Shipper shipper);
        //void Delete(Shipper shipper);
        //void Update(Shipper shipper);
        Shipper Find(int id);
        //IEnumerable<Shipper> GetAll();
    }

    public class EFShipperRepository : IShipperRepository
    {
        public void Add(Shipper shipper)
        {
            throw new NotImplementedException();
        }

        public Shipper Find(int id)
        {
            throw new NotImplementedException();
        }
    }

    public class DbShipperRepository : IShipperRepository
    {
        private readonly SqlConnection connection;

        public DbShipperRepository(SqlConnection connection)
        {
            this.connection = connection;
        }

        public void Add(Shipper shipper)
        {
            string sql = "INSERT INTO Shippers(CompanyName, Phone) " +
                         "VALUES(@companyname, @phone)";

            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("@id", shipper.Id);
            cmd.Parameters.AddWithValue("@companyname", shipper.CompanyName);
            cmd.Parameters.AddWithValue("@phone", shipper.Phone);
            cmd.ExecuteNonQuery();
        }

        public Shipper Find(int id)
        {
            string sql = "SELECT * FROM Shippers " +
                         "WHERE ShipperID = @id";

            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("@id", id);
            
            Shipper shipper = null;
            using (IDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    int shipperId = reader.GetInt32(0);
                    string companyName = reader["CompanyName"].ToString();
                    string phone = reader["Phone"].ToString();

                    shipper = new Shipper { Id = shipperId, CompanyName = companyName, Phone = phone };
                }
            }

            return shipper;
        }
    }

    public class InMemoryShipperRepository : IShipperRepository
    {
        private static int nextId = 1;
        private Dictionary<int, Shipper> shippers = new Dictionary<int, Shipper>();

        public void Add(Shipper shipper)
        {
            shippers[nextId++] = shipper;
        }

        public Shipper Find(int id)
        {
            return shippers[id];
        }
    }
}
