﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace DP.Structural.Facade.TableDataGateway
{
    class Client
    {
        IShipperRepository _shipperRepository;

        public Client(IShipperRepository _shipperRepository)
        {
            this._shipperRepository = _shipperRepository;
        }

        public void TestGateway()
        {
            Shipper s1 = _shipperRepository.Find(1);
            Console.WriteLine("{0} {1} {2}", s1.Id, s1.CompanyName, s1.Phone);

            Shipper s2 = new Shipper { Id = 4, CompanyName = "TestShipper", Phone = "+48 12 444-00-11" };
            _shipperRepository.Add(s2);

            Shipper s3 = _shipperRepository.Find(4);
            Console.WriteLine("{0} {1} {2}", s3.Id, s3.CompanyName, s3.Phone);
        }
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            SqlConnectionStringBuilder connStrBld = new SqlConnectionStringBuilder();
            connStrBld.DataSource = "localhost";
            connStrBld.InitialCatalog = "Northwind";
            connStrBld.IntegratedSecurity = true;

            SqlConnection connection = new SqlConnection(connStrBld.ConnectionString);
            connection.Open();

            IShipperRepository _shipperRepository = new DbShipperRepository(connection);
            Client client = new Client(_shipperRepository);
            client.TestGateway();
        }
    }
}
