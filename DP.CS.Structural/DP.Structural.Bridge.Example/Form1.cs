﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Structural.Bridge.Example
{
    public partial class Form1 : Form
    {
        Document doc;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Graphics g = pnlCanvas.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            doc = new Document(new WinFormsShapeImpl(g));
            doc.Load(@"..\..\graphics.xml");
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            doc.Show();
        }
    }
}
