﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{

public class Rectangle : Shape
    {
        private int width;
        public int Width 
        { 
            get
            {
                return width;
            }

            set
            {
                width = value;
                int x = GetPoint(0).X;
                int y = GetPoint(0).Y;
                SetPoint(1, new Point(x + width, y));
                SetPoint(2, new Point(x + width, y + Height));
                SetPoint(3, new Point(x, y + Height));
            }
        }

        private int height;
        public int Height
        {
            get 
            { 
                return height; 
            }

            set
            {
                height = value;
                int x = GetPoint(0).X;
                int y = GetPoint(1).Y;

                SetPoint(1, new Point(x + Width, y));
                SetPoint(2, new Point(x + Width, y + height));
                SetPoint(3, new Point(x, y + height));
            }
        }

        public Rectangle() : this(0, 0, 0, 0)
        {
        }

        public Rectangle(int x, int y, int width, int height)
        {
            AddPoint(new Point(x, y));
            AddPoint(new Point(x + width, y));
            AddPoint(new Point(x + width, y + height));
            AddPoint(new Point(x, y + height));
        }

        public override void Draw()
        {
            for (int i = 0; i < NumberOfPoints() - 1; ++i)
                Implementation.DrawLine(GetPoint(i), GetPoint(i + 1));
            Implementation.DrawLine(GetPoint(NumberOfPoints()-1), GetPoint(0));
        }

        public class Factory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement rectElement = (XElement)args[0];
                int x = int.Parse(rectElement.Element("Point").Element("X").Value);
                int y = int.Parse(rectElement.Element("Point").Element("Y").Value);
                int width = int.Parse(rectElement.Element("Width").Value);
                int height = int.Parse(rectElement.Element("Height").Value);

                return new Rectangle(x, y, width, height);
            }
        }
    }
}