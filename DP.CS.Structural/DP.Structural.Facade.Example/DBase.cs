﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;

namespace DP.Structural.Facade.Example
{
    public abstract class DBase
    {
        protected OleDbConnection conn;

        //------
        private void openConnection()
        {
            if (conn.State == ConnectionState.Closed)
                conn.Open();
        }

        //------
        private void closeConnection()
        {
            if (conn.State == ConnectionState.Open)
                conn.Close();
        }

        //------
        public DataTable openTable(string tableName)
        {
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataTable dtable = null;
            string query = "Select * from " + tableName;
            adapter.SelectCommand = new OleDbCommand(query, conn);
            DataSet dset = new DataSet("mydata");
            
            openConnection();
            adapter.Fill(dset);
            dtable = dset.Tables[0];
            
            return dtable;
        }

        //------
        public DataTable openQuery(string query)
        {
            OleDbDataAdapter dsCmd = new OleDbDataAdapter();
            DataSet dset = new DataSet();    //create a dataset 
            DataTable dtable = null;          //declare a data table 
            
            //create the command 
            dsCmd.SelectCommand = new OleDbCommand(query, conn);
            openConnection();   //open the connection 
            //fill the dataset 
            dsCmd.Fill(dset, "mine");
            //get the table 
            dtable = dset.Tables[0];
            closeConnection();              //always close it 
            return dtable;                  //and return it 
        }

        //------
        public void openConnection(string connectionString)
        {
            conn = new OleDbConnection(connectionString);
        }

        //------
        public OleDbConnection getConnection()
        {
            return conn;
        }
    }


    public class AccessDbase : DBase
    {
        string connectionString;

        public AccessDbase(string dbName)
        {
            connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dbName;
            openConnection(connectionString);
        }
    }

    public class SqlServerDBase : DBase
    {
        string connectionString;

        //-----
        public SqlServerDBase(String dbName)
        {
            connectionString = @"Provider=SqlOleDB; Data Source=localhost\SqlExpress;"
                + "Initial Catalog=" + dbName + ";Integrated Security=SSPI";

            openConnection(connectionString);
        }

        //-----
        public SqlServerDBase(string dbName, string serverName, string userid, string pwd)
        {
            connectionString = "Provider=SqlOleDB; Data Source=" + serverName
                + ";Initial Catalog=" + dbName + "; user Id=" + userid + "; Password=" + pwd;
            
            openConnection(connectionString);
        }
    }
}
