﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Drawing
{
    [Serializable()]
    public struct Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("[{0}, {1}]", X, Y);
        }
    }

    public interface IShape
    {
        void Draw();
        void Move(int dx, int dy);
        IShape Clone();
    }

    [Serializable()]
    public abstract class Shape : IShape
    {
        private List<Point> points = new List<Point>();

        protected Point GetPoint(int index)
        {
            return points[index];
        }

        protected void SetPoint(int index, Point pt)
        {
            points[index] = pt;
        }

        protected int NumberOfPoints()
        {
            return points.Count;
        }

        protected void AddPoint(Point pt)
        {
            points.Add(pt);
        }

        #region IShape Members

        public abstract void Draw();

        public virtual void Move(int dx, int dy)
        {
            for (int i = 0; i < points.Count; ++i)
            {
                SetPoint(i, new Point(points[i].X + dx, points[i].Y + dy));
            }
        }

        public virtual IShape Clone()
        {
            IShape clonedShape = null;
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                clonedShape = formatter.Deserialize(stream) as IShape;
            }

            return clonedShape;
        }

        #endregion        
    }

    public abstract class ShapeFactory
    {
        private static IDictionary<string, ShapeFactory> creators = new Dictionary<string, ShapeFactory>();

        protected abstract IShape CreateShape(params object[] args);

        public static IShape Create(string id, params object[] args)
        {
            return creators[id].CreateShape(args);
        }

        public static void Register(string id, ShapeFactory creator)
        {
            creators.Add(id, creator);
        }

        public static void Unregister(string id)
        {
            creators.Remove(id);
        }
    }
}
