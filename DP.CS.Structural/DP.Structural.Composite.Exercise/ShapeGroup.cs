﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    // implementacja kompozytu
    [Serializable]
    class ShapeGroup : IShape
    {
        List<IShape> _shapes = new List<IShape>();

        public void Add(IShape shape)
        {
            _shapes.Add(shape);
        }

        public void Remove(IShape shape)
        {
            _shapes.Remove(shape);
        }

        public void Draw()
        {
            foreach (var s in _shapes)
            {
                s.Draw();
            }
        }

        public void Move(int dx, int dy)
        {
            foreach (var s in _shapes)
            {
                s.Move(dx, dy);
            }
        }

        public IShape Clone()
        {
            IShape clonedShape = null;
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                clonedShape = formatter.Deserialize(stream) as IShape;
            }

            return clonedShape;
        }

        public class Factory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement groupElement = (XElement)args[0];

                var shapeElements = groupElement.Elements("Shape");

                ShapeGroup group = new ShapeGroup();

                foreach (XElement element in shapeElements)
                {
                    string id = element.Attribute("Id").Value;
                    try
                    {
                        IShape s = ShapeFactory.Create(id, element);
                        group.Add(s);
                    }
                    catch (KeyNotFoundException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                return group;
            }
        }
 
    }
}
