﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Decorator.TheoryCode
{
    class Client
    {
        internal void Run(Component c)
        {
            c.Operation();
        }
    }
}
