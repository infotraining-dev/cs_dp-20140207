﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    class CoffeeBuilder
    {
        private Coffee _coffee;

        public CoffeeBuilder CreateBase<TBase>() where TBase : Coffee, new()
        {
            _coffee = new TBase();

            return this;
        }

        public CoffeeBuilder Add<TCondiment>() where TCondiment : CoffeeDecorator, new()
        {
            CoffeeDecorator decorated = new TCondiment();
            decorated.Coffee = _coffee;
            _coffee = decorated;

            return this;
        }

        public Coffee GetCoffee()
        {
            return _coffee;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Coffee myCoffee; // = new Whipped(new Whisky(new Whisky(new Whisky(new Espresso()))));
            CoffeeBuilder coffeeBld = new CoffeeBuilder();
            coffeeBld
                .CreateBase<Espresso>()
                .Add<Whisky>().Add<Whisky>().Add<Whipped>();

            myCoffee = coffeeBld.GetCoffee();

            Console.WriteLine("Coffee: {0} - Price: {1:c}", myCoffee.GetDescription(), myCoffee.GetTotalPrice());
            myCoffee.Prepare();

            Console.WriteLine();

            Service srv = new Service();
            LoggedService lgsrv = new LoggedService(srv);
            Client client = new Client();
            client.Run(lgsrv);

            Console.ReadKey();
        }
    }

    class Service
    {
        public virtual void DoWork()
        {
            Console.WriteLine("Service.DoWork");
        }
    }

    class LoggedService : Service
    {
        private Service _service;

        public LoggedService(Service srv)
        {
            _service = srv;
        }

        public override void DoWork()
        {
            Console.WriteLine("Log: " + DateTime.Now);
            base.DoWork();
        }
    }

    class Client
    {
        public void Run(Service srv)
        {
            srv.DoWork();
        }
    }
}
