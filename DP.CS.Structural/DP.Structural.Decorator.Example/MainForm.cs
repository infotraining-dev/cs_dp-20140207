﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PhotoLibrary;
using System.IO;

namespace PhotoDecorator
{
    public partial class MainForm : Form
    {
        List<IThumbnail> photographs = new List<IThumbnail>();

        public MainForm()
        {
            InitializeComponent();
            this.Paint += new PaintEventHandler(MainForm_Paint);
        }

        void MainForm_Paint(object sender, PaintEventArgs e)
        {
            pnlCanvas.Refresh();
        }

        private void btnFindDir_Click(object sender, EventArgs e)
        {
            if (folderBrowserDlg.ShowDialog() == DialogResult.OK)
            {
                photographs.Clear();
                string path = folderBrowserDlg.SelectedPath;
                txtDir.Text = path;
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                var jpgFiles = from file in dirInfo.GetFiles()
                               where file.Name.EndsWith(".jpg") || file.Name.EndsWith(".png")
                               orderby file.Name
                               select file;

                foreach (FileInfo fi in jpgFiles)
                {
                    string fileName = fi.FullName;
                    photographs.Add(
                        new TaggedPhoto(
                            new FramedPhoto(
                                new ThumbnailComponent(fi.FullName, 200, 200, true),  210, 210), 
                                fileName)

                    );
                }
            }

            this.Refresh();
        }

        private void pnlCanvas_Paint(object sender, PaintEventArgs e)
        {
            int startX = 50;
            int startY = 50;

            foreach (IThumbnail photo in photographs)
            {
                photo.Draw(e.Graphics, startX, startY);
                startX += photo.Width + 20;

                if (startX + photo.Width > pnlCanvas.Width)
                {
                    startX = 50;
                    startY += photo.Height + 20;
                }
            }
        }
    }
}
