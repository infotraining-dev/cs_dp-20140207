﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Adapter.Example.Sorting
{
    class Employee : IComparable<Employee>
    {
        public int id;
        public string name;

        public Employee(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public override string ToString()
        {
            return name + " " + id.ToString();
        }

        #region IComparable<Employee> Members

        public int CompareTo(Employee other)
        {
            return this.id.CompareTo(other.id);
        }

        #endregion
    }

    class EmployeeByNameComparer : IComparer<Employee>
    {
        #region IComparer<Employee> Members

        public int Compare(Employee x, Employee y)
        {
            return x.name.CompareTo(y.name);
        }

        #endregion
    }

    class Program
    {
        static void Main(string[] args)
        {
            Employee[] emps = { new Employee(1, "Nowak"), new Employee(4, "Adamska"), new Employee(2, "Kowalski") };

            Array.Sort(emps);

            foreach (Employee e in emps)
                Console.WriteLine(e);

            Console.WriteLine("------");

            Array.Sort(emps, new EmployeeByNameComparer());

            foreach (Employee e in emps)
                Console.WriteLine(e);

            Console.ReadKey();
        }
    }
}
