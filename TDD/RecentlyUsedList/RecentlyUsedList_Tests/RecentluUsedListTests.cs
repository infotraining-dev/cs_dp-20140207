﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RecentlyUsedListImpl;

namespace RecentlyUsedList_Tests
{
    #region Description of Kata - Recently Used List
    //Recently Used List :

    //    Develop a recently-used-list class to hold strings 
    //    uniquely in Last-In-First-Out order.

    //    o) The most recently added item is first, the least
    //       recently added item is last.

    //    o) Items can be looked up by index, which counts from zero.

    //    o) Items in the list are unique, so duplicate insertions
    //       are moved rather than added.

    //    o) A recently-used-list is initially empty.

    //    Optional extras

    //    o) Null insertions (empty strings) are not allowed.

    //    o) A bounded capacity can be specified, so there is an upper
    //       limit to the number of items contained, with the least
    //       recently added items dropped on overflow.
    #endregion

    [TestFixture]
    public class WhenUsingANewRecentlyUsedList
    {
        private RecentlyUsedList _list;
        private const string item = "item";

        [SetUp]
        public void BeforeTest()
        {
            _list = new RecentlyUsedList();
        }

        [Test]
        public void ShouldBeEmpty()
        {
            Assert.AreEqual(0, _list.Count);
        }

        [Test]
        public void ShouldResizeAfterInsertingAnItem()
        {
            _list.Add(item);

            Assert.AreEqual(1, _list.Count);
        }

        [Test]
        public void ShouldStoreInsertedItemAtIndexZero()
        {
            _list.Add(item);

            Assert.AreSame(item, _list[0]);
        }
    }

    public class PopulatedRecentlyUsedList
    {
        protected RecentlyUsedList _list;
        protected readonly string[] _items;
        protected readonly int _size;
        protected readonly int _capacity;

        public PopulatedRecentlyUsedList(string items, int size, int capacity = Int32.MaxValue)
        {
            _items = items.Split(',');
            _size = size;
            _capacity = capacity;
        }

        [SetUp]
        public void PopulateList()
        {
            _list = new RecentlyUsedList(_capacity);

            foreach (var item in _items)
            {
                _list.Add(item);
            }
        }
    }

    [TestFixture("item1,item2,item3,item4", 4)]
    public class WhenUsingAPopulatedRecentlyUsedList : PopulatedRecentlyUsedList
    {
        public WhenUsingAPopulatedRecentlyUsedList(string items, int size)
            : base(items, size)
        {
        }

        [Test]
        public void ShouldReturnANumberOfElements()
        {
            Assert.AreEqual(_size, _list.Count);
        }

        [Test]
        public void ShouldStoreItemsInLIFOOrder()
        {
            CollectionAssert.AreEqual(_items.Reverse().ToList(), _list);
        }

        [Test]
        public void ItemsShouldBeLookedUpByIndex()
        {
            for (int i = 0; i < _size; ++i)
            {
                Assert.AreEqual(_list[i], _items[(_size - 1) - i]);
            }
        }
    }

    [TestFixture("item1,item2,item3,item4", 4)]
    public class WhenInsertingADuplicateIntoRecentlyUsedList : PopulatedRecentlyUsedList
    {
        public WhenInsertingADuplicateIntoRecentlyUsedList(string items, int size)
            : base(items, size)
        {
        }

        [Test]
        public void ShouldStoreUniqueItems()
        {
            _list.Add(_items[1]);

            CollectionAssert.AllItemsAreUnique(_list);
        }

        [Test]
        public void ShouldMoveAnItemToTheFront()
        {
            _list.Add(_items[1]);

            string[] expectedOrder = { "item2", "item4", "item3", "item1" };

            CollectionAssert.AreEqual(expectedOrder, _list);
        }
    }

    [TestFixture]
    public class WhenInsertingNullValueToRecentlyUsedList
    {
        private RecentlyUsedList _list;

        [Test]
        public void ShouldThrowAgrumentNullException()
        {
            RecentlyUsedList list = new RecentlyUsedList();

            Assert.Throws<ArgumentNullException>(
                () => list.Add(null));
        }
    }

    [TestFixture("item1,item2,item3,item4", 4, 4)]
    public class WhenInsertingAnItemToFullRecentlyUsedList : PopulatedRecentlyUsedList
    {
        public WhenInsertingAnItemToFullRecentlyUsedList(string items, int size, int capacity)
            : base(items, size, capacity)
        {
        }

        [Test]
        public void ShouldNotChangeTheSize()
        {
            int size = _list.Count;

            _list.Add("item5");

            Assert.AreEqual(size, _list.Count);
        }

        [Test]
        public void ShouldDropTheLastItemOnOverflow()
        {
            _list.Add("item5");

            var expectedList = new List<string>() { "item5", "item4", "item3", "item2" };

            CollectionAssert.AreEqual(expectedList, _list);
        }
    }
}
