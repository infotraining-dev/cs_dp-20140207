﻿namespace StubsVsMocks
{
    public interface IWarehouse
    {
        void Add(string productName, int quantity);
        int GetInventory(string productName);
        void Remove(string productName, int quantity);
        bool HasInventory(string productName, int quantity);
    }
}