﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HandRolledMocks
{
    class DummyDependency : IDependency
    {
        public int GetValue(string s)
        {
            return 1;
        }

        public void CallMeFirst()
        {
            throw new NotImplementedException();
        }

        public int CallMeTwice(string s)
        {
            throw new NotImplementedException();
        }

        public void CallMeLast()
        {
            throw new NotImplementedException();
        }
    }
}
