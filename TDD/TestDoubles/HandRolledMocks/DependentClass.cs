﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandRolledMocks
{
    class DependentClass
    {
        private readonly IDependency _dependancy;

        public DependentClass(IDependency dependancy)
        {
            _dependancy = dependancy;
        }

        public int GetValue(string s)
        {
            return _dependancy.GetValue(s);
        }

        public void CallMeFirst()
        {
            _dependancy.CallMeFirst();
        }

        public void CallMeLast()
        {
            _dependancy.CallMeLast();
        }

        public int CallMeTwice(string s)
        {
            return _dependancy.CallMeTwice(s);
        }
    }
}
