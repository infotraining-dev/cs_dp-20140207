﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleLogger;
using CustomerRepositoryEF;
using CustomerService;
using CustomerService.ExternalServices;
using Ninject;
using Ninject.Modules;

namespace CustomerApp
{
    class AppModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICustomerRepository>().To<CustomersRespository>();
            Bind<ILogger>().To<Logger>();
            Bind<CustomersDataContext>().ToSelf();
        }
    }


    class Program
    {
        static StandardKernel kernel = new StandardKernel(new AppModule());

        static void GetAllCustomers(CustomersService service)
        {
            var customers = service.GetAll();

            foreach (var c in customers)
                Console.WriteLine(c.ID + " " + c.FirstName + " " + c.LastName);
        }

        static void GetCustomerById(CustomersService service)
        {
            try
            {
                var customer = service.FindById(13);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exeption: " + e.Message);
            }
        }

        static void Main(string[] args)
        {
            CustomersService service = kernel.Get<CustomersService>();

            //GetAllCustomers(service);

            GetCustomerById(service);
        }
    }
}
