﻿using System;

namespace CustomerService
{
    public class CustomerNotFoundException : ApplicationException
    {
        public CustomerNotFoundException(string message)
            : base(message)
        {
        }
    }
}