﻿using System.Collections.Generic;

namespace CustomerService
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAll();
        Customer Find(int id);
    }
}
