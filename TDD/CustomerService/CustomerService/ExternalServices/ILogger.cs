﻿namespace CustomerService.ExternalServices
{
    public interface ILogger
    {
        void Error(string message);
    }
}
