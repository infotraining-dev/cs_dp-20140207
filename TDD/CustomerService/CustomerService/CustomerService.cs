﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerService.ExternalServices;

namespace CustomerService
{
    /* Klasa CustomerService
     * 1 - lista klientów w kolejności alfabetycznej
     * 2 - lista klientów o podanym nazwisku
     * 3 - dane klienta o podanym id
     *      - jeśli brak klienta o podanym id - to wyjatek + zapis do dziennika logów 
     */


    public class CustomersService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ILogger _logger;

        public CustomersService(ICustomerRepository customerRepository, ILogger logger)
        {
            _customerRepository = customerRepository;
            _logger = logger;
        }

        public IEnumerable<Customer> GetAll()
        {
            return _customerRepository.GetAll().OrderBy(c => c.LastName);
        }

        public IEnumerable<Customer> FindByName(string name)
        {
            return _customerRepository.GetAll().Where(c => c.LastName == name);
        }

        public Customer FindById(int id)
        {
            Customer foundCustomer = _customerRepository.Find(id);

            if (foundCustomer == null)
            {
                _logger.Error(string.Format("Customer with ID={0} not found", id));
                throw new CustomerNotFoundException(string.Format("Customer ID={0} not found", id));
            }

            return _customerRepository.Find(id);
        }
    }
}
