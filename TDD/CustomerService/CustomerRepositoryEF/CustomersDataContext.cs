﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerService;

namespace CustomerRepositoryEF
{
    public class CustomersDataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; } 
    }

    public class CustomersRespository : ICustomerRepository
    {
        private CustomersDataContext _dbContext;

        public CustomersRespository(CustomersDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Customer> GetAll()
        {
            return _dbContext.Customers;
        }

        public Customer Find(int id)
        {
            return _dbContext.Customers.Find(id);
        }
    }
}
