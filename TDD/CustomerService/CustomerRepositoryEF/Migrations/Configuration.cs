using CustomerService;

namespace CustomerRepositoryEF.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CustomerRepositoryEF.CustomersDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CustomersDataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            
            context.Customers.AddOrUpdate(p => p.ID,
                new Customer() { FirstName = "Jan", LastName = "Kowalski"},
                new Customer() { FirstName = "Adam", LastName = "Kowalski" },
                new Customer() { FirstName = "Zenon", LastName = "Nowak"},
                new Customer() { FirstName = "Anna", LastName = "Nowak"});
        }
    }
}
