using System;
using System.Collections.Generic;
using System.Linq;
using CustomerService;
using CustomerService.ExternalServices;
using Moq;
using NUnit.Framework;

namespace CustomerService_Tests
{

    public class With_CustomerService
    {
        protected CustomersService _service;
        protected Mock<ICustomerRepository> _mqCustomerRepository;
        protected List<Customer> _customers = CustomerTestHelpers.CreateCustomers();
        protected Mock<ILogger> _mqLogger;

        [SetUp]
        public void InitSUT()
        {
            _mqCustomerRepository =
                new Mock<ICustomerRepository>();
  
            _mqCustomerRepository
                .Setup(m => m.GetAll()).Returns(_customers);

            _mqLogger = new Mock<ILogger>();

            _service =
                new CustomersService(_mqCustomerRepository.Object, _mqLogger.Object);
        }
    }

    [TestFixture]
    public class WhenGettingAllCustomers : With_CustomerService
    {
        [Test]
        public void SholudReturnCustomersInAlphabeticalOrder()
        {
            // Act
            var result = _service.GetAll();

            // Assert
            CollectionAssert.AreEqual(result, 
                _customers.OrderBy(c => c.LastName).ToList());
        }
    }

    [TestFixture]
    public class WhenGettingCustomersByName : With_CustomerService
    {
        [Test]
        public void And_customers_with_name_exists_Should_return_matching_customers()
        {
            // Act
            string name = "L3";
            var result = _service.FindByName(name);

            // Assert
            CollectionAssert.AreEqual(result, _customers.Where(c => c.LastName == name));
        }

        [Test]
        public void And_customers_with_name_does_not_exists_Should_return_empty_collection()
        {
            // Act
            string name = "L4";
            var result = _service.FindByName(name);

            // Assert
            Assert.That(result.Count(), Is.EqualTo(0));
        }
    }
    
    public static class CustomerTestHelpers
    {
        public static List<Customer> CreateCustomers()
        {
            return new List<Customer>()
            {
                new Customer() { ID = 1, FirstName = "F1", LastName = "L3"}, 
                new Customer() { ID = 2, FirstName = "F2", LastName = "L1"},
                new Customer() { ID = 3, FirstName = "F3", LastName = "L2"},
                new Customer() { ID = 3, FirstName = "F4", LastName = "L3"},
            };
        }
    }

    public class WhenGettingCustomerById : With_CustomerService
    {
        [Test]
        public void And_customer_with_id_exists_Should_return_found_customer()
        {
            int id = 2;
            Customer customer = 
                new Customer() {ID = 2, FirstName = "FN", LastName = "LN"};

            _mqCustomerRepository
                .Setup(m => m.Find(id)).Returns(customer);

            var result = _service.FindById(id);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ID, Is.EqualTo(id));
        }

        [Test]
        public void And_customer_with_id_does_not_exist_Should_throw_exception()
        {
            int id = 13;

            _mqCustomerRepository.Setup(m => m.Find(id)).Returns(() =>  null);

            Assert.Throws<CustomerNotFoundException>(() => _service.FindById(id));
        }

        [Test]
        public void And_customer_with_id_does_not_exist_Should_log_a_message_with_error()
        {
            const int id = 13;

            _mqCustomerRepository.Setup(m => m.Find(id)).Returns(() => null);

            try
            {
                _service.FindById(id);
            }
            catch(CustomerNotFoundException e)
            {}

            _mqLogger.Verify(m => m.Error(string.Format("Customer with ID={0} not found", id)), Times.Once());
        }
    }    
}