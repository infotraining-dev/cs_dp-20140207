﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerService.ExternalServices;

namespace ConsoleLogger
{
    public class Logger : ILogger
    {
        public void Error(string message)
        {
            Console.WriteLine("Log: " + message);
        }
    }
}
