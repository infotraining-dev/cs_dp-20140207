﻿using System.Configuration;
using Ninject;
using Ninject.Modules;

namespace BusinessApplication.Core
{
    public class BusinessService
    {
        private IDataAccessComponent _dataAccessComponent;
        private IWebServiceProxy _webServiceProxy;
        private ILoggingComponent _loggingComponent;
        

        public BusinessService(IDataAccessComponent dataAccessComponent, IWebServiceProxy webServiceProxy, ILoggingComponent loggingComponent)
        {
            _dataAccessComponent = dataAccessComponent;
            _webServiceProxy = webServiceProxy;
            _loggingComponent = loggingComponent;
        }
    }

    public class AppModule : NinjectModule
    {
        public override void Load()
        {
            string connectionString = "connStr";
            Bind<IDataAccessComponent>()
                .To<DataAccessComponent>()
                .WithConstructorArgument("connectionString", connectionString);
            Bind<IWebServiceProxy>()
                .To<WebServiceProxy>()
                .WithConstructorArgument("webServiceAddress", "http://webservice.bt.com:8080");
            Bind<ILoggingComponent>().To<LoggingComponent>().InSingletonScope();
            Bind<LoggingDataSink>().ToSelf();
        }
    }

    class Program
    {
        public static void Main()
        {
            #region BeforeDIContainer
            string connectionString = "connStr";
            BusinessService service1 = new BusinessService(
                                            new DataAccessComponent(connectionString),
                                            new WebServiceProxy("http://service.com"),
                                            new LoggingComponent(
                                                new LoggingDataSink()));
            #endregion

            #region AfterDIContainer

            var kernel = new StandardKernel(new AppModule());

            BusinessService service2 = kernel.Get<BusinessService>();

            #endregion

        }
    }
}
