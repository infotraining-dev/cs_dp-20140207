﻿using System;

namespace BusinessApplication.Core
{
    public interface ILoggingComponent
    {
    }

    public interface IWebServiceProxy
    {
    }

    public interface IDataAccessComponent
    {
    }

    internal class LoggingDataSink
    {
    }

    internal class LoggingComponent : ILoggingComponent
    {
        public LoggingComponent(LoggingDataSink loggingDataSink)
        {
            Console.WriteLine("Loggin Component");
        }
    }

    internal class WebServiceProxy : IWebServiceProxy
    {
        public WebServiceProxy(string webServiceAddress)
        {
            Console.WriteLine("WebServiceProxy");
        }
    }

    internal class DataAccessComponent : IDataAccessComponent
    {
        public DataAccessComponent(string connectionString)
        {
            Console.WriteLine("DAL");
        }
    }

}