﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    public class BowlingGame
    {

        private int[] _rolls = new int[21];
        private int _currentThrow;
        private const int MAX_FRAME_INDEX = 10;
        private const int MAX_POINTS_IN_FRAME = 10;

        public int Score
        {
            get
            {
                int score = 0;
                int frameIndex = 0;

                for (int frame = 0; frame < MAX_FRAME_INDEX; ++frame)
                {
                    if (IsStrike(frameIndex))
                    {
                        score += StrikeBonus(frameIndex);
                        frameIndex++;
                    }
                    else
                    {
                        score += SumOfPointsInFrame(frameIndex);

                        if (IsSpare(frameIndex))
                        {
                            score += SpareBonus(frameIndex);
                        }

                        frameIndex += 2;
                    }
                }

                return score;
            }
        }

        private int StrikeBonus(int frameIndex)
        {
            return MAX_POINTS_IN_FRAME + _rolls[frameIndex + 1] + _rolls[frameIndex + 2];
        }

        private bool IsStrike(int frameIndex)
        {
            return _rolls[frameIndex] == MAX_POINTS_IN_FRAME;
        }

        private int SpareBonus(int frameIndex)
        {
            return _rolls[frameIndex + 2];
        }

        private int SumOfPointsInFrame(int frameIndex)
        {
            return _rolls[frameIndex] + _rolls[frameIndex + 1];
        }

        private bool IsSpare(int frameIndex)
        {
            return _rolls[frameIndex] + _rolls[frameIndex + 1] == MAX_POINTS_IN_FRAME;
        }

        public void Roll(int pins)
        {
            _rolls[_currentThrow++] = pins;
        }
    }
}
