﻿using NUnit.Framework;

namespace Bowling.Tests
{
    [TestFixture]
    [Category("Bowling Game")]
    public class BowlingGameTests
    {
        private BowlingGame _game;

        [SetUp]
        public void StartBowlingGame()
        {
            _game = new BowlingGame();
        }

        [Test]
        [Category("Init Game")]
        public void WhenGameStartsScoreShouldBeZero()
        {
            Assert.AreEqual(0, _game.Score);
        }

        [Test]
        public void WhenAllThrowsInGutterScoreShouldBeZero()
        {
            // Act
            RollMany(20, 0);
            
            // Assert
            Assert.AreEqual(0, _game.Score);
        }

        [TestCase(1, 20)]
        [TestCase(2, 40)]
        [TestCase(4, 80)]
        public void WhenAllThrowsNoMarkScoreShouldBeSumOfPins(int pins, int score)
        {
            // Act
            RollMany(20, pins);

            // Assert
            Assert.That(_game.Score, Is.EqualTo(score));
        }

        [Test]
        public void WhenSpareNextThrowIsCountedTwice()
        {
            RollSpare();
            _game.Roll(3);
            RollMany(17, 0);

            Assert.That(_game.Score, Is.EqualTo(16));
        }

        [Test]
        public void WhenStrikeTwoNextThrowsAreCountedTwice()
        {
            RollStrike();
            RollMany(2, 3);
            RollMany(16, 0);

            Assert.That(_game.Score, Is.EqualTo(22));
        }

        [Test]
        public void WhenPerfectGameScoreShouldBe300()
        {
            for(int i = 0; i < 12; ++i)
                RollStrike();

            Assert.That(_game.Score, Is.EqualTo(300));
        }

        private void RollStrike()
        {
            _game.Roll(10);
        }
        
        private void RollMany(int count, int pins)
        {
            for (int i = 0; i < count; ++i)
                _game.Roll(pins);
        }

        private void RollSpare()
        {
            _game.Roll(5);
            _game.Roll(5);  
        }
    }
}

