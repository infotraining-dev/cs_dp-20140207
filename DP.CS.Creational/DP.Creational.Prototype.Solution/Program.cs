﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.FactoryMethod.Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape.CloneFactory.Register("Circle", new Circle());
            Shape.CloneFactory.Register("Rectangle", new Rectangle());
            Shape.CloneFactory.Register("Image", new Image());

            string[] shapesId = { "Circle", "Rectangle", "Rectangle", "Circle", "Image" };

            List<Shape> shapes = new List<Shape>();

            foreach (string id in shapesId)
                shapes.Add(Shape.CloneFactory.CreateShape(id));

            foreach (Shape shape in shapes)
                shape.Draw();

            Console.ReadKey();
        }
    }
}
