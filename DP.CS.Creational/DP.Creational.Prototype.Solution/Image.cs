﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DP.Creational.FactoryMethod.Exercise
{
    [Serializable()]
    class Image : Shape
    {
        public string Path { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        public Image()
        {
            Path = "c:\\default.jpg";
        }

        public Image(string path, int width, int height)
        {
            Path = path;
            Width = width;
            Height = height;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing Image: " + Path);
        }

        public override Shape Clone()
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream, this);
            stream.Seek(0, SeekOrigin.Begin);

            Shape copy = formatter.Deserialize(stream) as Shape;
            stream.Close();

            return copy;
        }
    }
}
