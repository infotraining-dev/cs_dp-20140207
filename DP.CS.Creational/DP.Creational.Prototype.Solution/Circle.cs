﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.FactoryMethod.Exercise
{
    // "Circle"
    public class Circle : Shape
    {
        public int Radius { get; set; }

        public Circle()
        {
        }

        public Circle(int x, int y, int r)
            : base(x, y)
        {
            Radius = r;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing Circle");
        }
    }
}
