﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.Singleton.TheoryCode
{
    class ConfigMgr
    {
        static ConfigMgr instance = new ConfigMgr();

        ConfigMgr()
        {
        }

        internal void Load()
        {
            Console.WriteLine("ConfigMgr.Load");
        }

        public static ConfigMgr Instance {
            get { return instance; }
    }
    }
}
