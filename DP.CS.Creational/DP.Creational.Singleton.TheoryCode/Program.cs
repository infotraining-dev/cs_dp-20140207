﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DP.Creational.Singleton.TheoryCode;

namespace SingletonPattern
{
    // "Singleton"
    public sealed class Singleton
    {
        private Singleton() { Console.WriteLine("SingletonPattern.Singleton initialized"); }  // prywatny konstruktor

        static readonly Singleton uniqueInstance = new Singleton();

        public static Singleton Instance
        {
            get { return uniqueInstance; }
        }

        public void DoSomething(string msg)
        {
            Console.WriteLine("Singleton's job: " + msg);
        }
    }
}

namespace LazySingletonPattern
{
    //// "Singleton"
    public class Singleton
    {
        private Singleton() { Console.WriteLine("LazySingletonPattern.Singleton initialized"); } // prywatny kontruktor

        // zagniezdzona klasa - lazy instantiation
        class SingletonCreator
        {
            static SingletonCreator() {}

            internal static readonly Singleton uniqueInstance = new Singleton();
        }

        public static Singleton Instance
        {
            get { return SingletonCreator.uniqueInstance; }
        }

        public void DoSomething(string msg)
        {
            Console.WriteLine("Singleton's job: " + msg);
        }
    }
}

namespace GenericSingleton
{
    class Singleton<T> where T : new()
    {
        private static T uniqueInstance = new T();

        public static  T Instance
        {
            get
            {
                return uniqueInstance;
            }
        }
    }

    class Test
    {
        public void DoSomething(string msg)
        {
            Console.WriteLine("Singleton's job: " + msg);
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Starting an app...");

        SingletonPattern.Singleton s1 = SingletonPattern.Singleton.Instance;
        SingletonPattern.Singleton s2 = SingletonPattern.Singleton.Instance;

        s1.DoSomething("First singleton");

        if (Object.ReferenceEquals(s1, s2))
            Console.WriteLine("Reference equals");
        else
            Console.WriteLine("Different instances");

        Console.WriteLine("\n---------------\n");


        LazySingletonPattern.Singleton s3;

        Console.WriteLine("Lazy instatation of singleton");
        Console.WriteLine("Press a Key");
        Console.ReadKey();
        s3 = LazySingletonPattern.Singleton.Instance;
        s3.DoSomething("Second singleton");

        Console.WriteLine("\n---------------\n");

        GenericSingleton.Singleton<GenericSingleton.Test>.Instance.DoSomething("Third singleton");

        Console.ReadKey();
    }

    class App
    {
        public void Run()
        {
            ConfigMgr conf = GetConfigMgr();
            conf.Load();
        }

        protected virtual ConfigMgr GetConfigMgr()
        {
            return ConfigMgr.Instance;
        }
    }
}

