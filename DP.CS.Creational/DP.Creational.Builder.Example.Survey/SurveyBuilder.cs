﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DP.Creational.Builder.Example.Survey
{
    public class SurveyAnwser
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public interface SurveyBuilder
    {
        void AddText(string question);
        void AddCheckBoxList(string question, SurveyAnwser[] anwsers );
        void AddRadioButtonList(string question, SurveyAnwser[] anwsers);
    }

    public class SurveyXmlParser
    {
        private SurveyBuilder builder;

        public SurveyXmlParser(SurveyBuilder builder)
        {
            this.builder = builder;
        }

        public void BuildSurvey(string path)
        {
            XElement root = XElement.Load(path);

            foreach (var element in root.Elements("Question"))
            {
                string type = element.Attribute("Type").Value;
                string text = element.Element("Text").Value;

                if (type == "TextBox")
                    builder.AddText(text);
                else
                {
                    var anwsers = element.Descendants("Anwser");
                    List<SurveyAnwser> surveyAnwsers = new List<SurveyAnwser>();

                    foreach (var a in anwsers)
                    {
                        surveyAnwsers.Add(new SurveyAnwser() { Text = a.Element("Text").Value, Value = a.Element("Value").Value });
                    }

                    if (type == "CheckBoxList")
                    {
                        builder.AddCheckBoxList(text, surveyAnwsers.ToArray());
                    }
                    else if (type == "RadioButtonList")
                    {
                        builder.AddRadioButtonList(text, surveyAnwsers.ToArray());
                    }
                }
            }
        }
    }

    public class WinFormsSurveyBuilder : SurveyBuilder
    {
        FlowLayoutPanel survey = new FlowLayoutPanel();

        public WinFormsSurveyBuilder()
        {
            survey.FlowDirection = FlowDirection.TopDown;
            survey.AutoSize = true;
        }

        public void AddText(string question)
        {
            FlowLayoutPanel pnl = new FlowLayoutPanel();
            pnl.FlowDirection = FlowDirection.LeftToRight;
            pnl.AutoSize = true;
            Label lblQuestion = new Label();
            lblQuestion.Text = question;
            TextBox txtAnwser = new TextBox();

            pnl.Controls.Add(lblQuestion);
            pnl.Controls.Add(txtAnwser);

            survey.Controls.Add(pnl);
        }

        public void AddCheckBoxList(string question, SurveyAnwser[] anwsers)
        {
            GroupBox gbxControl = new GroupBox();
            FlowLayoutPanel pnl = new FlowLayoutPanel();
            gbxControl.Text = question;
            gbxControl.AutoSize = true;
            gbxControl.Dock = DockStyle.Fill;
            gbxControl.Margin = new System.Windows.Forms.Padding(10);
            pnl.AutoSize = true;
            pnl.Dock = DockStyle.Fill;
            gbxControl.Controls.Add(pnl);

            foreach (var a in anwsers)
            {
                CheckBox cbxItem = new CheckBox();
                cbxItem.Name = "cbx" + a.Value;
                cbxItem.Text = a.Text;
                pnl.Controls.Add(cbxItem);
            }

            survey.Controls.Add(gbxControl);
        }

        public void AddRadioButtonList(string question, SurveyAnwser[] anwsers)
        {
            GroupBox gbxControl = new GroupBox();
            FlowLayoutPanel pnl = new FlowLayoutPanel();
            gbxControl.Text = question;
            gbxControl.AutoSize = true;
            gbxControl.Dock = DockStyle.Fill;
            gbxControl.Margin = new System.Windows.Forms.Padding(10);
            pnl.AutoSize = true;
            pnl.Dock = DockStyle.Fill;
            gbxControl.Controls.Add(pnl);

            foreach (var a in anwsers)
            {
                RadioButton rbtItem = new RadioButton();
                rbtItem.Name = "cbx" + a.Value;
                rbtItem.Text = a.Text;
                pnl.Controls.Add(rbtItem);
            }

            survey.Controls.Add(gbxControl);
        }

        public Control GetSurvey()
        {
            return survey;
        }
    }
}
