﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    public class Image : Shape
    {
        private string _path;

        public Image(int x1, int y1, string path)
        {
            AddPoint(new Point(x1, y1));
            _path = path;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing an image {0} at {1}", _path, GetPoint(0));
        }

        public class Factory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement imageElement = (XElement)args[0];
                int x = int.Parse(imageElement.Element("Point").Element("X").Value);
                int y = int.Parse(imageElement.Element("Point").Element("Y").Value);
                string path = imageElement.Element("Path").Value;

                return new Image(x, y, path);
            }
        }

    }
}
