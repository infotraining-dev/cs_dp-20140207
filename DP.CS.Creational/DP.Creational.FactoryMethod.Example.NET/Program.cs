﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes;

namespace DP.Creational.FactoryMethod.Example.NET
{
    public class Program
    {
        static Program()
        {
            Console.WriteLine("Startup of application");
            System.Reflection.Assembly shapeAssembly = System.Reflection.Assembly.Load("Shapes");

            var shapeTypes = from type in shapeAssembly.GetTypes() 
                             where (!type.FullName.EndsWith("+Factory") && (!type.IsAbstract))
                             select type;

            foreach(Type s in shapeTypes)
            {
                Type typeFactory = shapeAssembly.GetType(s.FullName + "+Factory");
                Shape.Factory sf = typeFactory.GetConstructor(new Type[0]).Invoke(new object[0]) as Shape.Factory;
                Console.WriteLine("Registering a factory {0} for {1}", sf, s.Name);
                Shape.Factory.Register(s.Name, sf);
            }
        }

        public static void Main()
        {
            Shape s = Shape.Factory.Create("Circle");
            s.Draw();
            s = Shape.Factory.Create("Image");
            s.Draw();

            Console.ReadKey();
        }
    }
}
