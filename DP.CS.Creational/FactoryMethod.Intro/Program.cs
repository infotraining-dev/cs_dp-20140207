﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Intro
{
    interface IProduct
    {
        void DoWork();
    }

    class ProductA : IProduct
    {
        public void DoWork()
        {
            Console.WriteLine("ProductA.DoWork()");
        }
    }

    class ProductB : IProduct
    {
        public void DoWork()
        {
            Console.WriteLine("ProductB.DoWork()");
        }
    }

    class ProductC : IProduct
    {
        public void DoWork()
        {
            Console.WriteLine("ProductC.DoWork()");
        }
    }

    interface IProductCreator
    {
        IProduct CreateProduct();
    }

    class ProductACreator : IProductCreator
    {

        public IProduct CreateProduct()
        {
            return new ProductA();
        }
    }

    class ProductBCreator : IProductCreator
    {
        public IProduct CreateProduct()
        {
            return new ProductB();
        }
    }

    class ProductCCreator : IProductCreator
    {
        public IProduct CreateProduct()
        {
            return new ProductC();
        }
    }


    class Client
    {
        private List<IProduct> _products = new List<IProduct>();
        private IProductCreator _creator;

        public Client(IProductCreator creator)
        {
            _creator = creator;
        }

        public void Init()
        {
            for (int i = 0; i < 10; ++i)
                _products.Add(_creator.CreateProduct());
        }

        public void Run()
        {   
            foreach(var p in _products)
                p.DoWork();
        }
    }

    class Factory
    {
        private IDictionary<string, IProductCreator> _creators = new Dictionary<string, IProductCreator>();

        public void RegisterCreator(string id, IProductCreator creator)
        {
            _creators.Add(id, creator);
        }

        public IProduct Create(string id)
        {
            return _creators[id].CreateProduct();
        }
    }

    class Program
    {
        //static IProduct Create(string id)
        //{
        //    if (id == "A")
        //        return new ProductA();
        //    else if (id == "B")
        //        return new ProductB();

        //    throw new ArgumentException("Invalid id");
        //}
        static void Bootstrap(Factory factory)
        {
            factory.RegisterCreator("A", new ProductACreator());
            factory.RegisterCreator("B", new ProductBCreator());
            factory.RegisterCreator("C", new ProductCCreator());
        }
        
        static void Main(string[] args)
        {
            Factory fact = new Factory();
            Bootstrap(fact);

            Client client = new Client(new ProductACreator());

            Console.WriteLine();

            IProduct p = fact.Create("A");
            p.DoWork();

            client.Init();
            client.Run();
        }
    }
}
