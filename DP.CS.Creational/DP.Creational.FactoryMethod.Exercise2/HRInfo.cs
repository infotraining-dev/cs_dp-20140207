﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.FactoryMethod.Exercise2
{
    public abstract class HRInfo
    {
        protected readonly Employee employee;

        public HRInfo(Employee e)
        {
            employee = e;
        }

        public abstract void Info();
    }

    public class StdInfo : HRInfo
    {
        public StdInfo(Employee e)
            : base(e)
        {
        }

        public override void Info()
        {
            Console.WriteLine("Standard information for employee: ");
            Console.WriteLine(" + " + employee.Description());
        }
    }

    public class TempInfo : HRInfo
    {
        public TempInfo(Employee e)
            : base(e)
        {
        }

        public override void Info()
        {
            Console.WriteLine("Standard information for employee: ");
            Console.WriteLine(" + " + employee.Description());
            Temp tmp = (Temp)employee;
            if (tmp.IsActive)
                Console.WriteLine(" + Active employee");
            else
                Console.WriteLine(" + Non-active employee");
        }
    }
}
