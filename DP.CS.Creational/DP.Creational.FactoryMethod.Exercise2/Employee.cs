﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.FactoryMethod.Exercise2
{
    public abstract class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public abstract string Description();
    }

    public class Salaried : Employee
    {
        public decimal Salary { get; set; }

        public Salaried(int id, string name)
            : base(id, name)
        {
        }

        public override string Description()
        {
            return string.Format("Salaried employee: {0}; Id: {1}", Name, Id);
        }
    }

    public class Hourly : Employee
    {
        public decimal HourlyRate { get; set; }

        public Hourly(int id, string name)
            : base(id, name)
        {
        }

        public override string Description()
        {
            return string.Format("Hourly employee: {0}; Id: {1}", Name, Id);
        }
    }

    public class Temp : Employee
    {
        public bool IsActive { get; set; }

        public Temp(int id, string name)
            : base(id, name)
        {
        }

        public override string Description()
        {
            return string.Format("Temporary employee: {0}; Id: {1}", Name, Id);
        }
    }
}

