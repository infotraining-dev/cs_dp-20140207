﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace Drawing
{
    public class Image : Shape
    {
        private byte[] imageBuffer;

        private string path;

        public string Path
        {
            get { return path; }

            set
            {
                path = value;

                loadFromFile(path);
            }
        }

        private void loadFromFile(string path)
        {
            using (FileStream stream = File.OpenRead(path))
            {
                int bytesToRead = (int)stream.Length;
                imageBuffer = new byte[bytesToRead];
                stream.Read(imageBuffer, 0, bytesToRead);
                Console.WriteLine("Loading an image - size: {0} bytes", bytesToRead);
            }
        }

        Image(int x, int y, string path)
        {
            AddPoint(new Point(x, y));

            loadFromFile(path);
        }

        public override void Draw()
        {
            Console.Write("Drawing an image at {0}: ", GetPoint(0));
            foreach (byte b in imageBuffer)
            {
                Console.Write((char)b);
            }
            Console.WriteLine();
        }

        public class Factory : Shape.Factory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement imageElement = (XElement)args[0];
                int x = int.Parse(imageElement.Element("Point").Element("X").Value);
                int y = int.Parse(imageElement.Element("Point").Element("Y").Value);
                string path = imageElement.Element("Path").Value;

                return new Image(x, y, path);
            }
        }
    }
}
