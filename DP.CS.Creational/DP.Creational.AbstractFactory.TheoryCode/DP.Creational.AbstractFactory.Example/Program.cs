﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.AbstractFactory.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            MonsterFactory factory = null;
            
            // choosing a level of game
            Console.WriteLine("Level EASY: 1");
            Console.WriteLine("Level DIE_HARD: 2");
            
            string chosenLevel = "";
            do
            {
                Console.Write("Choose level of game: ");
                chosenLevel = Console.ReadLine();
            } while (!((chosenLevel == "1") || (chosenLevel == "2")));

            switch(chosenLevel)
            {
                case "1":
                    factory = new SillyMonsterFactory();
                    break;
                case "2":
                    factory = new BadMonsterFactory();
                    break;
            }

            // creating monsters
            List<Enemy> enemies = new List<Enemy>();
            Random random = new Random(); 

            for (int i = 0; i < 10; ++i)
            {
                int rand = random.Next(10);
                if (rand < 3)
                    enemies.Add(factory.CreateSoldier());
                else if ((rand >= 3) && (rand <= 6))
                    enemies.Add(factory.CreateMonster());
                else
                    enemies.Add(factory.CreateSuperMonster());
            }

            // fighting
            foreach (Enemy e in enemies)
                e.Fight();


            Console.ReadKey();
        }
    }
}
