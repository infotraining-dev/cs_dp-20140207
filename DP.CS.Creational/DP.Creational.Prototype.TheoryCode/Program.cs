﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DP.Creational.Prototype.TheoryCode
{
    /// <summary>
    /// Prototype Design Pattern
    /// </summary>

    [Serializable()]
    public abstract class Prototype<T>
    {
        // shallow copy
        public T Clone()
        {
            return (T)this.MemberwiseClone();
        }

        // deep copy
        public T DeepCopy()
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, this);
            stream.Seek(0, SeekOrigin.Begin);
            T copy = (T)formatter.Deserialize(stream);
            stream.Close();
            return copy;
        }
    }

    [Serializable()]
    class ConcretePrototype1 : Prototype<ConcretePrototype1>
    {
        private List<string> members = new List<string>();

        public void AddMember(string member)
        {
            members.Add(member);
        }

        public override string ToString()
        {
            string str = "ConcretePrototype1 Members: ";

            foreach (string s in members)
                str += s + " ";

            return str;
        }
    }

    [Serializable()]
    class ConcretePrototype2 : Prototype<ConcretePrototype2>
    {
      private List<int> numbers = new List<int>();

      public void AddNumber(int number)
      {
        numbers.Add(number);
      }

      public override string ToString()
      {
        string str = "ConcretePrototype2 Numbers: ";

        foreach (int n in numbers)
          str += n.ToString() + " ";

        return str;
      }

    }


    class Program
    {
        static void Main(string[] args)
        {
            // creating an original object cp1
            ConcretePrototype1 cp1 = new ConcretePrototype1();
            cp1.AddMember("#1");
            cp1.AddMember("#2");
            cp1.AddMember("#3");
            Console.WriteLine("Original object cp1: " + cp1.ToString());


            // clonning of prototype
            ConcretePrototype1 p1 = cp1.DeepCopy();
            Console.WriteLine("Clone of object p1: " + p1.ToString());


            // checking a deep copy
            p1.AddMember("#4");
            Console.WriteLine("After modification of a clone: ");
            Console.WriteLine("Original object cp1: " + cp1.ToString());
            Console.WriteLine("Clone of object p1: " + p1.ToString());

            // creating an original object cp1
            ConcretePrototype2 cp2 = new ConcretePrototype2();
            cp2.AddNumber(13);
            cp2.AddNumber(7);
            cp2.AddNumber(69);
            Console.WriteLine("Original object cp2: " + cp2.ToString());

            // clonning of prototype
            ConcretePrototype2 p2 = cp2.DeepCopy();
            Console.WriteLine("Clone of object p2: " + p2.ToString());

            Console.ReadKey();
        }
    }
}
