﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawing
{
    public struct Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("[{0}, {1}]", X, Y);
        }
    }

    public interface IShape
    {
        void Draw();
        void Move(int dx, int dy);
        IShape Clone();
    }

    public abstract class Shape : IShape
    {
        private List<Point> points = new List<Point>();

        protected Point GetPoint(int index)
        {
            return points[index];
        }

        protected void SetPoint(int index, Point pt)
        {
            points[index] = pt;
        }

        protected int NumberOfPoints()
        {
            return points.Count;
        }

        protected void AddPoint(Point pt)
        {
            points.Add(pt);
        }

        #region IShape Members

        public abstract void Draw();

        public virtual void Move(int dx, int dy)
        {
            for (int i = 0; i < points.Count; ++i)
            {
                SetPoint(i, new Point(points[i].X + dx, points[i].Y + dy));
            }
        }

        public virtual IShape Clone()
        {
            throw new NotImplementedException();
        }

        #endregion

        public abstract class CloneFactory // TODO
        {
        }
    }
}
