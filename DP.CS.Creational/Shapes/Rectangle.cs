﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes
{
    // "Rectangle"
    public class Rectangle : Shape
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public Rectangle()
        {
        }

        public Rectangle(int x, int y, int width, int height)
            : base(x, y)
        {
            Width = width;
            Height = height;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing Rectangle");
        }

        public new class Factory : Shape.Factory
        {
            protected override Shape CreateShape()
            {
                return new Rectangle();
            }
        }
    }
}
