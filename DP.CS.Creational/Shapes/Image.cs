﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes
{
    public class Image : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("Drawing an Image");
        }

        public new class Factory : Shape.Factory
        {
            protected override Shape CreateShape()
            {
                return new Image();
            }
        }
    }
}
