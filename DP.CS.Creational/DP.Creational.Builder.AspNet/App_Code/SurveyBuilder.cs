﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.UI;


namespace SurveyBuilder.App_Code
{
    public class SurveyAnwser
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public interface SurveyBuilder
    {
        void AddText(string question);
        void AddCheckBoxList(string question, SurveyAnwser[] anwsers);
        void AddRadioButtonList(string question, SurveyAnwser[] anwsers);
    }

    public class SurveyXmlParser
    {
        private SurveyBuilder builder;

        public SurveyXmlParser(SurveyBuilder builder)
        {
            this.builder = builder;
        }

        public void BuildSurvey(string path)
        {
            XElement root = XElement.Load(path);

            foreach (var element in root.Elements("Question"))
            {
                string type = element.Attribute("Type").Value;
                string text = element.Element("Text").Value;

                if (type == "TextBox")
                    builder.AddText(text);
                else
                {
                    var anwsers = element.Descendants("Anwser");
                    List<SurveyAnwser> surveyAnwsers = new List<SurveyAnwser>();

                    foreach (var a in anwsers)
                    {
                        surveyAnwsers.Add(new SurveyAnwser() { Text = a.Element("Text").Value, Value = a.Element("Value").Value });
                    }

                    if (type == "CheckBoxList")
                    {
                        builder.AddCheckBoxList(text, surveyAnwsers.ToArray());
                    }
                    else if (type == "RadioButtonList")
                    {
                        builder.AddRadioButtonList(text, surveyAnwsers.ToArray());
                    }
                }
            }
        }
    }

    public class WebFormsSurveyBuilder : SurveyBuilder
    {
        Panel survey = new Panel();

        public void AddText(string question)
        {
            Label lblQuestion = new Label();
            lblQuestion.Text = question;
            lblQuestion.Width = 100;
            TextBox txtAnwser = new TextBox();
            Literal ltrBreak = new Literal();
            ltrBreak.Text = "<br/><br/>";
            survey.Controls.Add(lblQuestion);
            survey.Controls.Add(txtAnwser);
            survey.Controls.Add(ltrBreak);
        }

        public void AddCheckBoxList(string question, SurveyAnwser[] anwsers)
        {
            Label lblQuestion = new Label();
            lblQuestion.Text = question + "<br/>";
            CheckBoxList cbxAnwsers = new CheckBoxList();
            cbxAnwsers.RepeatDirection = RepeatDirection.Horizontal;
            foreach (var a in anwsers)
            {
                ListItem lstItem = new ListItem(a.Text, a.Value);
                cbxAnwsers.Items.Add(lstItem);
            }
            Literal ltrBreak = new Literal();
            ltrBreak.Text = "<br/>";

            survey.Controls.Add(lblQuestion);
            survey.Controls.Add(cbxAnwsers);
            survey.Controls.Add(ltrBreak);
        }

        public void AddRadioButtonList(string question, SurveyAnwser[] anwsers)
        {
            Label lblQuestion = new Label();
            lblQuestion.Text = question + "<br/>";
            RadioButtonList rblAnwsers = new RadioButtonList();
            rblAnwsers.RepeatDirection = RepeatDirection.Horizontal;
            foreach (var a in anwsers)
            {
                ListItem lstItem = new ListItem(a.Text, a.Value);
                rblAnwsers.Items.Add(lstItem);
            }
            Literal ltrBreak = new Literal();
            ltrBreak.Text = "<br/>";

            survey.Controls.Add(lblQuestion);
            survey.Controls.Add(rblAnwsers);
            survey.Controls.Add(ltrBreak);
        }

        public Control GetSurvey()
        {
            return survey;
        }
    }
}